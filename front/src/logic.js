const express = require('express');
const app = express();
const fs = require('fs');
const {Client} = require('pg');
const bodyParser = require('body-parser');  //POSTのために必要らしい
app.use(bodyParser.urlencoded({ extended: true }));  //POSTのために必要らしい
const route = express.Router();

//DB定義
const dbConnectionConfig={
    user: 'postgres',
    host: 'postgres-dbs',
    database: "postgres",
    password: null,
    port: 5432
};

//ルーティング
// app.get("/",(req,res)=>{
//     console.log(__dirname);
//     res.status(200).sendFile(__dirname+"/index.html");
// })

app.route("/task_count/")
    .get((req,res)=> {
    res.set("Access-Control-Allow-Origin","*");
    const client = new Client (dbConnectionConfig);
    client.connect();
    const selectQuery = "SELECT DISTINCT "+req.query.columns.join(",")+" FROM work_time_day "+(req.query.criteria?req.query.criteria+" ":" ");
    client.query(selectQuery)
        .then(result=>{
            console.log(result);
            res.status(200).send(result);
        })
        .catch(e=>{
            console.error(e.stack);
            res.status(500).send("error message: "+ e.message);
        });
    })
    .post((req,res)=>{
        res.setHeader("Content-Type","text/plain");
        res.set("Access-Control-Allow-Origin","*");
        const date=new Date();
        //タスク名と日付登録
        const client = new Client(dbConnectionConfig);
        client.connect()
        const dateInDay=formatDateInDay(date);
        const insertQuery = {
            text: 'INSERT INTO work_time_day(task_name, date_) VALUES($1,$2)',
            values: [req.body.data,dateInDay]
        }
        console.log(insertQuery["text"]+":"+insertQuery.values);
        client.query(insertQuery)
            .then(result => {
                console.log(result);
                res.send("タスク名登録のレスポンスです");//statusを入れないとブラウザ側でres.statusCodeとしてはキャッチできない？
            })
            .catch(e => {
                console.error(e.stack);
                res.status(500).send("error message: "+ e.message);//statusがないとDB登録失敗の部分に拾われる？
            });
    })

    app.post("/task_total/",(req,res)=>{
        res.set("Access-Control-Allow-Origin","*");
        const client = new Client(dbConnectionConfig);
        client.connect();
        let startDay=formatDateInDay(new Date(req.body.startDateInfo["date"]));
        let taskTimeInhOnStartDay=parseFloat(req.body.startDateInfo["task_time"]);
        let endDay=formatDateInDay(new Date(req.body.endDateInfo["date"]));
        let taskTimeInhOnEndDay=parseInt(req.body.endDateInfo["task_time"]);
        let task_name=req.body.task_name;
        
        
        // const updateQuery = "UPDATE work_time_day SET task_time=task_time + "
        //     + taskTimeInhOnStartDay + " WHERE task_name = '"+ task_name + "' AND date_='"+startDay+ "'";
        console.log("tasktimeは"+taskTimeInhOnStartDay);
        console.log("tasktimeは"+taskTimeInhOnEndDay);
        if(taskTimeInhOnStartDay!=0){    
            const updateQuery1 = "INSERT INTO work_time_day(task_name,date_,task_time) VALUES ('"
                +task_name+"','"+startDay+"',"+taskTimeInhOnStartDay+") ON CONFLICT ON CONSTRAINT work_time_day_pkey DO UPDATE SET task_time=work_time_day.task_time + "
                +taskTimeInhOnStartDay;
            console.log(updateQuery1)

            client.query(updateQuery1)
            .then(result => {
                console.log(result);
                res.status(200).send("DB更新成功。"+task_name);
            })
            .catch(e => {
                console.error(e.stack);
                res.status(500).send("error message: "+ e.message);
            });
        }
        //日をまたいでいたらinsertで作成して作業時間更新クエリ作成
        if(taskTimeInhOnEndDay){

            const  updateQuery2 ={
                text:'INSERT INTO work_time_day(task_name, date_,task_time) VALUES($1,$2,$3)',
                values:[task_name,endDay,taskTimeInhOnEndDay]
            };
            client.query(updateQuery2)
                .catch(e => {
                    console.error(e.stack);
                    res.status(500).send("error message: "+ e.message);
                });
        }
    })
    
    ;

    //関数定義

    //return date like "20190724"
    function formatDateInDay(date){
        let formattedDateInday="";
        const year = date.getFullYear();
        const month = ("00"+(date.getMonth()+1)).slice(-2);
        const day = ("00"+date.getDate()).slice(-2);
        formattedDateInDay=year+month+day;
        return formattedDateInDay;
    }


    // function autoInsert(task_name,){
    //     const autoInsertQuery = {
    //         text:'INSERT INTO work_time_day(task_name, date_) VALUES($1,$2)',
    //         values:[task_name,endDay]
    //     }
    //     client.query(autoInsertQuery)
    //         .catch(e=>{
    //             console.error(e.stack);
    //             res.status(500).send("error message: "+ e.message);
    //         })
    // }
app.listen(3000);