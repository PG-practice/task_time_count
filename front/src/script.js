const baseUrl = "http://127.0.0.1:30000/";

//タスク登録時にエンターキーを受け付け
$("#task_name").keypress(
    function(event){
        if(event.keyCode==13){
            dbRegister();
        }
    }
)

//セレクトボックス作成
$(function(){
    const columns=["task_name"];
    dbRetrieve(columns).then(result=>{
        result.forEach(element => {
            const each_task_name=element[columns[0]];
            $("select[name=task_list]")
                .append("<option value="+each_task_name+">"+each_task_name+"</option>");
        })
    });
});

//リスト作成
$(function(){
    const columns=["task_name","date_::timestamptz","task_time"];
    let fromDate = new Date();
    fromDate.setDate(fromDate.getDate()-10);
    const criteria="WHERE date_ >= "+"'"+formatDateInDay(fromDate)+"'";
    dbRetrieve(columns,criteria).then(result=>{
        let str = "";
        result.forEach(element=>{
            str+="<tr><td>"+element[columns[0]]+"</td>+<td>"+element[columns[1].substring(0,columns[1].length-13)].slice(0,10)+"</td>+<td style=\"text-align:right;\">"+element[columns[2]]+"</td></tr>"
        })
        $("div table").append(str);
    })
})


// 関数群---------------
function dbRegister(){
    let data=document.getElementById("task_name").value;
    $.post(baseUrl+"task_count/",{"data":data})
        .done(function(res,status,xhr){
            console.log(res+status+xhr);
            if (xhr.status==200){
                alert("DB登録完了: "+res);
                console.log(res);
            } else {
                alert("DB登録失敗..."+res);
                console.log(res.status+res);
            }

        })
        .fail(function( jqXHR, textStatus, errorThrown ){
            alert("ajax通信失敗(insert): "+jqXHR.responseText);
            console.log(jqXHR);
        })
};

const dbRetrieve=(columns, ...args)=>{
    const criteria = args[0];//可変長でDBの検索条件を設定可能
    return new Promise((resolve,reject)=>{
        $.get(baseUrl+"task_count/",{"columns":columns,"criteria":criteria})
            .done(res=>{
                const result=res.rows;
                resolve(result);
        })
            .fail(function( jqXHR, textStatus, errorThrown ){
                alert("ajax通信失敗(select): "+jqXHR.responseText);
                console.log(jqXHR);
                return;
            });
    });
};

//開始・終了ボタン押下時の関数
{//変数のスコープを制限するための{}
let startTime;
let timerId;
let nowTime;
let task_name;
let passedTimeIns;
let isProcessingFlag=false;
function startTime_(){
    if (!(isProcessingFlag && $("#select_task_list").val()==task_name)){
        isProcessingFlag=true;
        task_name = $("#select_task_list").val();
        clearInterval(timerId);
        startTime = new Date();
        nowTime = new Date();
        timerId = setInterval(() => {
            nowTime = new Date();
            let passedTimeInms = nowTime - startTime;
            passedTimeIns=passedTimeInms/1000.0;
            let tArr = ms_to_HMSformat(passedTimeIns);
            let HMSformatpassedTime=tArr.join(":");
            //24時間以上は禁止
            if (tArr[0]>=24){endTime_();}
            $("#passedTimeDisplay span").html(task_name+"  "+HMSformatpassedTime);
        }, 1000);
    }
}

function endTime_(){
    clearInterval(timerId);
    let tArr = ms_to_HMSformat(passedTimeIns);
    let HMSformatpassedTime=tArr.join(":");
        if (task_name!=null){
            alert(task_name+"を"+HMSformatpassedTime+"やりました。");
        }
    
    //startTime.getDate()!=nowTime.getDate()なら日付が変わっている
    //{昨日の作業時間:nowTimeの日付0時-startTime, 今日の作業時間:nowTime-nowTImeの日付0時}をdataに追加
    let taskTimeInhOnStartDay;
    let taskTimeInhOnEndDay;
    if (startTime.getDate()==nowTime.getDate()){
        taskTimeInhOnStartDay = Math.round((nowTime-startTime)/(3600*1000.0)*100)/100;
        console.log(taskTimeInhOnStartDay);
    } else if ((startTime.getDate()+1)==nowTime.getDate()){
        let nowDate=(new Date()).setHours(0,0,0,0);
        taskTimeInhOnStartDay = Math.round((nowTime-nowDate)/(3600*1000.0)*100)/100;
        taskTimeInhOnEndDay = Math.round((nowDate-startTime)/(3600*1000.0)*100)/100;
    } else {
        alert("endTime()で24時間以上連続作業をしたエラー");
    }
    
    
    $.ajax({
        url:baseUrl+"task_total/",
        data:{
            "task_name":task_name,
            "startDateInfo":{"date":startTime, "task_time":taskTimeInhOnStartDay},
            "endDateInfo":{"date":nowTime, "task_time":taskTimeInhOnEndDay}
            },
        method:"POST",
    })
    .then(result=>{
        console.log(result);
    })
    .fail(function( jqXHR, textStatus, errorThrown ){
        alert("aja通信失敗(update): "+jqXHR.responseText);
        console.log(jqXHR);
    });
    isProcessingFlag=false;
    task_name=null;
}


/*  
    s単位→h:m:s(時間・分・秒)への変換
    return [h,m,s] (String[]型)
*/
let ms_to_HMSformat = function(passedTimeIns){
    let h=Math.floor(passedTimeIns/3600);
        h=("00"+h).slice(-2);
    let m=Math.floor((passedTimeIns%3600)/60);
        m=("00"+m).slice(-2);
    let s=Math.floor((passedTimeIns%60));
        s=("00"+s).slice(-2);
    return [h,m,s];
}

}

//return date like "20190724"
function formatDateInDay(date){
    let formattedDateInday="";
    const year = date.getFullYear();
    const month = ("00"+(date.getMonth()+1)).slice(-2);
    const day = ("00"+date.getDate()).slice(-2);
    formattedDateInDay=year+month+day;
    return formattedDateInDay;
}
//SQL文を打てるようにする
//テーブルを見られるようにする(DBのGUIツール)
//*****end*****開始→終了→開始の順に押したときに2回目のカウントが始まらない
//セレクトボックスはSet使えばリストのDB通信の結果を使えるので通信を減らせる
//タスクは過去10日分の表示になっている。開始日を選択できるようにすること