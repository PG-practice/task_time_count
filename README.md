## About
simple application to use kubernetes, docker, Node.js, postgreSQL, jQuery. 

[task_count_app_schematic20190821.pdf](/uploads/12fb2cf97ee4f3a2a62bb743e07f3563/task_count_app_schematic20190821.pdf)  
#### One of my questions (tell me why...)
With Chrome on ubuntu 18.4 LTS,  `http://localhost:30000/src/` cannot be accessed even though `http://127.0.0.1:30000/src/` did always work well (FireFox allows the host name `localhost` but a CORS problem remains unsolved).   
Incidentally, with Chrome `ubuntu` (or IP`127.0.1.1`) could also work.


## Function 

- register task name
- count work time
- show (task name,date, work time)


## How to deploy

if you use Microk8s, use `forDeploy.sh`. In case for other distribution of k8s, need to do the following:  
1. docker image build  
    ```
    sudo docker build -t front_express ./front  
    sudo docker build -t postgres_db ./DB
    ```
2. deploy into kubernetes  
    ```
    kubectl create -f deployment.yaml 
    ```
3. access with chrome  
    `http://127.0.0.1:30000/src/`

In k8s, this reference may help.  
https://stackoverflow.com/questions/50671576/how-can-i-use-a-local-docker-image-in-kubernetes

## Other
This article helped me a lot when DNS resolver did not work well.
https://qiita.com/fastwind/items/21f8a8f3b867cd0d5ed3
