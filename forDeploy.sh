#!/bin/bash

echo "image build"
sudo docker build -t front_express ./front
sudo docker build -t postgres_db ./DB

echo "import local docker images to microk8s"
sudo docker save postgres_db > postgres_db.tar
sudo docker save front_express > front_express.tar
microk8s.ctr -n k8s.io image import postgres_db.tar
microk8s.ctr -n k8s.io image import front_express.tar

microk8s.kubectl delete deployment front-express
microk8s.kubectl delete service front-expresss
microk8s.kubectl delete deployment postgres-db
microk8s.kubectl delete service postgres-dbs

echo "deploy"
microk8s.kubectl create -f deployment.yaml 

if [ $? = 1 ];then
    microk8s.kubectl replace -f deployment.yaml
fi